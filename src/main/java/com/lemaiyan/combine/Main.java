package com.lemaiyan.combine;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;


public class Main {
    private static void printOutOrders(HashMap<Integer, Order> orders) {
        orders.forEach((k, v) -> System.out.println("Index : " + k + " Order : " + v));
    }

    public static void main(String[] args) {
        System.out.println("Welcome to add combiner");
        Scanner sc = new Scanner(System.in);
        String name, message;
        double originLatitude, originLongitude, destinationLatitude, destinationLongitude;
        int userCommand;
        OrderCombination combiner = new OrderCombination();
        message = "Enter:\n0 to quit\n1 to continue\n2 to pop the combined orders\n3 to peek the combined orders:" +
                "\n4 display all Combined orders in the queue";
        loop:
        while (true) {
            System.out.println("Please enter to continue:");
            sc.nextLine();
            System.out.println("Please enter the order name:");
            name = sc.nextLine();

            System.out.println("Please enter the order origin latitude:");
            originLatitude = sc.nextDouble();
            System.out.println("Please enter the order origin longitude:");
            originLongitude = sc.nextDouble();

            System.out.println("Please enter the order destination latitude:");
            destinationLatitude = sc.nextDouble();
            System.out.println("Please enter the order destination longitude:");
            destinationLongitude = sc.nextDouble();

            Geolocation origin = new Geolocation(originLatitude, originLongitude);
            Geolocation destination = new Geolocation(destinationLatitude, destinationLongitude);
            Order newOrder = new Order(name, origin, destination);
            combiner.addOrder(newOrder);
            System.out.println(message);
            userCommand = sc.nextInt();
            switch (userCommand) {
                case 0:
                    break loop;
                case 1:
                    continue loop;
                case 2:
                    printOutOrders(combiner.pop());
                    continue loop;
                case 3:
                    printOutOrders(combiner.peek());
                    continue loop;
                case 4:
                    LinkedList<HashMap<Integer, Order>> queue;
                    queue = combiner.getAllCombinedOrders();
                    int index = 0;
                    for(HashMap<Integer, Order> orders : queue){
                        System.out.println("Queue index: "+ index++);
                        printOutOrders(orders);
                    }
            }
        }
    }
}
