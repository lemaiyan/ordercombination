package com.lemaiyan.combine;

import java.util.Objects;

public class Geolocation {
    private double latitude;
    private double longitude;

    /***
     * Constructor
     * @param latitude double
     * @param longitude double
     */
    public Geolocation(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /***
     * Get Geolocation latitude
     * @return double
     */
    public double getLatitude() {
        return latitude;
    }

    /***
     * Set Geolocation latitude
     * @param latitude double
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /***
     * Get Geolocation Longitude
     * @return double
     */
    public double getLongitude() {
        return longitude;
    }

    /***
     * Set Geolocation longitude
     * @param longitude double
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /***
     * Override equals method
     * @param obj Geolocation
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if (!(obj instanceof Geolocation)) {
            return false;
        }
        Geolocation geolocation = (Geolocation) obj;
        return latitude == geolocation.latitude &&
                longitude == geolocation.longitude;

    }

    /***
     * Override hashCode method
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    /***
     * Override toString
     * @return String
     */
    @Override
    public String toString() {
        return "Latitude: "+ this.getLatitude() + ", Longitude: " + this.getLongitude();
    }

    /***
     * Get a String array of the latitude and longitude
     * @return String
     */
    public String[] toArray(){
        String locationString = this.getLatitude() + "," + this.getLongitude();
        return new String[]{locationString};
    }
}
