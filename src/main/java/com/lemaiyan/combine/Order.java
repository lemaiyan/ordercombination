package com.lemaiyan.combine;

import java.util.Date;

public class Order {
    private String orderId;
    private Geolocation origin;
    private Geolocation destination;
    private double distance;
    private double cost;
    private Date orderTime;

    /***
     * Constructor
     * @param orderId OrderID
     * @param origin origin Geolocation
     * @param destination destination Geolocation
     */
    public Order(String orderId, Geolocation origin, Geolocation destination) {
        this.orderId = orderId;
        this.origin = origin;
        this.destination = destination;
        this.orderTime = new Date();
    }

    /***
     * Get Order Id
     * @return String
     */
    public String getOrderId() {
        return orderId;
    }

    /***
     * Get Order Origin
     * @return Location
     */
    public Geolocation getOrigin() {
        return origin;
    }

    /***
     * get Order Destination
     * @return Location
     */
    public Geolocation getDestination() {
        return destination;
    }

    /***
     * Get Order Cost
     * @return double
     */
    public double getCost() {
        return cost;
    }

    /***
     * Set Order Cost
     * @param cost double
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /***
     * Get Distance
     * @return double
     */
    public double getDistance() {
        return distance;
    }

    /***
     * Set Distance
     * @param distance double
     */
    public void setDistance(double distance) {
        this.distance = distance;
    }

    /***
     * Get order date
     * @return Date
     */
    public Date getOrderTime() {
        return orderTime;
    }

    /***
     * Override to String method
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder stringValue = new StringBuilder("{");
        stringValue.append("orderId: "+ this.getOrderId());
        stringValue.append(", origin: "+ this.getOrigin());
        stringValue.append(", destination: "+ this.getDestination());
        stringValue.append(", distance: "+ this.getDistance());
        stringValue.append(", cost: "+ this.getCost());
        stringValue.append("}");
        return stringValue.toString();
    }
}
