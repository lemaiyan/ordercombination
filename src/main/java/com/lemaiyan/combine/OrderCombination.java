package com.lemaiyan.combine;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DistanceMatrix;

import java.util.HashMap;
import java.util.LinkedList;

public class OrderCombination {

    private static final int COST_PER_KM = 30;
    /*
    The time interval in seconds of which to consider so
    as to combine Orders
     */
    private static final int TIME_INTERVAL = 60;
    private static final int MAX_NEARBY_DISTANCE_IN_KM = 2;

    private LinkedList<HashMap<Integer, Order>> combinedOrders = new LinkedList<>();
    private int currentHashMapIndex = 0;
    private int currentLinkIndex = 0;
    private GeoApiContext context;

    /***
     * Constructor
     */
    public OrderCombination() {
        this.context = new GeoApiContext.Builder()
                .apiKey("AIzaSyDSy6UpeVGwnGtWDq5pFoENr7B379svgQA")
                .build();
    }

    /***
     * Takes an order and if there are other existing orders
     * it checks if they can be combined. If they can combines them and
     * return true else returns false
     * @param order Order
     * @return boolean
     */
    public boolean addOrder(Order order) {
        HashMap<Integer, Order> orderHashMap;
        Order updatedOrder = this.updateOrderDetails(order);
        /*
        if no other order has been added
        add this as the first order
         */
        if (combinedOrders.size() == 0) {
            orderHashMap = new HashMap<>();
            orderHashMap.put(currentHashMapIndex++, updatedOrder);
            combinedOrders.add(currentLinkIndex, orderHashMap);
            return true;
        } else {
            long timeToStopCombining;
            orderHashMap = combinedOrders.get(currentLinkIndex);
            Order firstOrder = orderHashMap.get(0);
            timeToStopCombining = firstOrder.getOrderTime().getTime() + TIME_INTERVAL;
            /*
            Check if the  order time us within the time interval for combination
            If not we start start combining the orders on the next index of the
            LinkedList and reset the currentHashMapIndex to 0
             */
            if (updatedOrder.getOrderTime().getTime() > timeToStopCombining) {
                currentLinkIndex++;
                currentHashMapIndex = 0;
                orderHashMap = new HashMap<>();
                /*
                  If the order can be combined add to the hashmap and update
                  the linked list and return true.
                 */
                if (ifOrdersAreCombinable(firstOrder, updatedOrder)) {
                    orderHashMap.put(currentHashMapIndex++, updatedOrder);
                    combinedOrders.add(currentLinkIndex, orderHashMap);
                    return true;
                }
            } else {
                /*
                  If the order can be combined add to the hashmap and update
                  the linked list and return true.
                 */
                if (ifOrdersAreCombinable(firstOrder, updatedOrder)) {
                    orderHashMap.put(currentHashMapIndex++, updatedOrder);
                    combinedOrders.add(currentLinkIndex, orderHashMap);
                    return true;
                }
            }

            return false;
        }
    }

    /***
     * Get all the combined orders
     * @return LinkedList
     */
    public LinkedList<HashMap<Integer, Order>> getAllCombinedOrders() {
        return this.combinedOrders;
    }

    /***
     * Get the first combined order in queue
     * @return HashMap
     */
    public HashMap<Integer, Order> pop() {
        return this.combinedOrders.pop();
    }

    /***
     * Teke a peek at the first combined Order in the queue
     * @return HashMap
     */
    public HashMap<Integer, Order> peek() {
        return this.combinedOrders.pop();
    }


    /*
        Private functions
     */

    /***
     * Checks whether the first order in the HashMap can be combined
     * with the new order by checking if the orders intersect, overlap
     * or nearby.
     * This is achieved by checking the the distance between the order origins
     * if it's not more than the MAX_NEARBY_DISTANCE_IN_KM the new order can
     * be combined with the other orders since they are either overlapping, intersecting or nearby
     * @param originalOrder Order
     * @param newOrder Order
     * @return boolean
     */
    private boolean ifOrdersAreCombinable(Order originalOrder, Order newOrder) {
        double distance = getDistance(
                originalOrder.getOrigin().toArray(),
                newOrder.getOrigin().toArray());
        // convert distance to kms
        distance /= 1000;
        return !(distance > MAX_NEARBY_DISTANCE_IN_KM);
    }

    /***
     * Updates the orders distance and cost
     * @param order Order
     * @return Order
     */
    private Order updateOrderDetails(Order order) {
        double distance = getDistance(
                order.getOrigin().toArray(),
                order.getDestination().toArray());
        double cost = distance * COST_PER_KM;
        order.setDistance(distance);
        order.setCost(cost);
        return order;
    }

    /***
     * Call Google Distance Matrix to get the distance between two locations
     * @param origins Order
     * @param destinations Order
     * @return double in metres
     */
    private double getDistance(String[] origins, String[] destinations) {
        try {
            //matrix.rows[0].elements[0].distance.inMeters
            DistanceMatrix matrix = DistanceMatrixApi.getDistanceMatrix(this.context, origins, destinations).await();
            return matrix.rows[0].elements[0].distance.inMeters / 1000.0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0;
        }
    }
}
